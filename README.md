# LEmobi

tracking shared mobility vehicles in leipzig.
optimized for use with a local SQLite3 database running on a raspberry pi.

currently supported operators (e-scooters):
- TIER
- VOI

providing support in the future:
- nextBike (free floating rental bicycles)
- Zoom (free floating electric scooters)
- cityFlitzer (free floating car sharing)

## Motivation

- tracking vehicle data by accessing publicly available API endpoints
- making shared mobility usage and demand more visible
- inferring potential routes with shortest path algorithms

![](./images/scooter_leipzig.png)

## Prerequisites

- SQLite3
- PostgreSQL with PostGIS extension installed for further processing (might be obsolete sometime)
- Python 3.7 or newer

The following Python packages are required to run this script:
- requests
- pandas
- sqlalchemy

## How to run

- Define your Sqlite3 DB path (default: `sqlite:////home/pi/Documents/scooter/scooter.db`). Change Line 9 accordingly.
```
9 .. sq = sqalchemy.creat_engine("sqlite://<YOUR-DB-PATH>")
```
- Get yourself an authKey for TIER and an authToken for VOI. Check https://github.com/ubahnverleih/WoBike for further instructions.
- Change line 56 and provide your authKey for the TIER API.
- Do the same on line 74 with your own authToken for the VOI API.

```
56 .. x_api_key = <YOUR-AUTHKEY>
.
.
74 .. voi_authToken = <YOUR-AUTHTOKEN>
```
## TODO
- stuff to make running this script more comfortable
- logging capabilities
- more advanced error handlings
