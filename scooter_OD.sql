WITH leipzig AS (SELECT row_number() over () as idx,
                        operator,
       "licensePlate",
       LAG(lat) OVER (partition by "licensePlate" order by request_time) AS sourceLat,
       LAG(lon) OVER (partition by "licensePlate" order by request_time) AS sourceLon,
       lat AS destinationlat,
       lon as destinationLon,
       LAG(request_time) OVER (partition by "licensePlate" order by request_time) AS sourceRequest,
       request_time AS destRequest,
       LAG("batteryLevel") OVER (partition by "licensePlate" order by request_time) AS sourceBattery,
       "batteryLevel" AS destBattery,
                        "lastActivity",
                        "lastRide",
       st_distance(st_transform(st_setsrid(st_makepoint(LAG(lat) OVER (partition by "licensePlate"),LAG(lon) OVER (partition by "licensePlate")),4326),25833),
       st_transform(st_setsrid(st_makepoint(lat,lon),4326),25833)) AS dist
       FROM scooter)
SELECT * FROM leipzig
WHERE dist > 100
ORDER BY "licensePlate"
