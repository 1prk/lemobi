import requests
import pandas as pd
import datetime
import time
import sqlalchemy
import logging

operators = ["TIER", "VOI"]
sq = sqlalchemy.create_engine("sqlite:////home/pi/Documents/scooter/scooter.db")

def voi_auth(voi_authToken):
    voi_authToken_url = "https://api.voiapp.io/v1/auth/session"
    voi_authToken_request = requests.post(voi_authToken_url, json={"authenticationToken": voi_authToken})
    if voi_authToken_request.status_code != 401:
        try:
            voi_accessToken = voi_authToken_request.json()['accessToken']
            voi_header = {"x-access-token": voi_accessToken}
            return voi_header
        except KeyError:
            print('invalid authToken, retry!')
    else:
        print('invalid authToken, retry!')
        return None


def voi(zone_id, authHeader): #default: 247 for leipzig
    voi_url = "https://api.voiapp.io/v1/rides/vehicles"
    voi_payload = {"zone_id": zone_id}
    voi_req = requests.get(voi_url, params=voi_payload, headers=authHeader)

    if voi_req.status_code == 401:
        print("error fetching {} vehicles - access to API denied! Check your authKey!".format(operators[1]))
        return None

    if (
        voi_req.status_code != 204 and
        voi_req.headers["content-type"].strip().startswith("application/json")
    ):
        try:
            voi_df = pd.json_normalize(voi_req.json()['data']['vehicles'])
            voi_df = voi_df[['location.lat', 'location.lng', 'battery', 'short']]
            voi_df[['lastActivity', 'lastRide']] = None
            voi_df['operator'] = operators[1]
            voi_df['request_time'] = datetime.datetime.now()
            voi_df.columns = ['lat', 'lon', 'batteryLevel', 'licensePlate', 'lastActivity', 'lastRide', 'operator', 'request_time']
            print("caught {} {} scooters".format(len(voi_df), operators[1]))
            voi_df.to_sql('scooters', con=sq, if_exists='append')
            return voi_df
        except ValueError:
            print("error fetching {} vehicles - no valid JSON response. Auth token may be invalid!".format(operators[1]))
            return None


def tier(zone_id):
    tier_url = "https://platform.tier-services.io/v1/vehicle"
    x_api_key = None #use your own API key!
    tier_header = {"X-Api-Key": x_api_key}
    tier_params = {"zoneId": zone_id}
    req = requests.get(tier_url, params=tier_params, headers=tier_header)
    res = req.json()
    tier_df = pd.json_normalize(res['data'])
    tier_df = tier_df[["attributes.lat", "attributes.lng", "attributes.batteryLevel", "attributes.licencePlate",
                  "attributes.lastLocationUpdate", "attributes.lastStateChange"]]
    tier_df[['operator', 'request_time']] = [operators[0], datetime.datetime.now()]
    tier_df.columns = ['lat', 'lon', 'batteryLevel', 'licensePlate', 'lastActivity', 'lastRide', 'operator',
                       'request_time']
    print("caught {} {} scooters".format(len(tier_df), operators[0]))
    tier_df.to_sql('scooters', con=sq, if_exists='append')
    return tier_df

def main():
    while True:
        #logging.basicConfig(filename=round(datetime.datetime.now().timestamp())+'_scraping.log', encoding='utf-8',level=logging.DEBUG)
        voi_authToken = None #use your own authTokens!

        voi('247', voi_auth(voi_authToken))
        tier("LEIPZIG")
        time.sleep(300)

if __name__ == "__main__":
    main()
